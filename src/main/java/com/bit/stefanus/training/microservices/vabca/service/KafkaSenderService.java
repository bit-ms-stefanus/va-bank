package com.bit.stefanus.training.microservices.vabca.service;

/**
 *
 * @author Stefanus
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bit.stefanus.training.microservices.vabca.dto.TagihanResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaSenderService {

    @Value("${kafka.topic.tagihan.response:tagihan-response-dev}")
    private String topicTagihanResponse;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void kirimTagihanResponse(TagihanResponse tagihanResponse) throws JsonProcessingException {
        String jsonResponse = objectMapper.writeValueAsString(tagihanResponse);
        log.info("Tagihan response : {}", jsonResponse);
        kafkaTemplate.send(topicTagihanResponse, jsonResponse);
    }
}
