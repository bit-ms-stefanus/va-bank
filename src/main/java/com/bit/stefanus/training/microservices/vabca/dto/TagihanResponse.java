package com.bit.stefanus.training.microservices.vabca.dto;

/**
 *
 * @author Stefanus
 */
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TagihanResponse {

    private String nomorTagihan;
    private String nama;
    private BigDecimal nilaiTagihan;
    private String bank = "BCA";
    private String nomorVa;
}