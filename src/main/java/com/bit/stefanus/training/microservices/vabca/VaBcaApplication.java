package com.bit.stefanus.training.microservices.vabca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaBcaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaBcaApplication.class, args);
	}

}
