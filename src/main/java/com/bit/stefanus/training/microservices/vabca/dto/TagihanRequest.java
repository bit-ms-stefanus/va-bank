package com.bit.stefanus.training.microservices.vabca.dto;

import java.math.BigDecimal;
import lombok.Data;

import java.time.LocalDate;

@Data
public class TagihanRequest {
    private String nomorTagihan;
    private String nama;
    private String email;
    private String noHp;
    private String keteranganTagihan;
    private BigDecimal nilaiTagihan;
    private LocalDate jatuhTempo;
}
